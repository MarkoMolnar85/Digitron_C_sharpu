Projekat digitron je kreiran u programskom jeziku C#. U okviru programa postoji nekoliko klasa. Digitron moze da racuna proste operacije kao mnozenje, deljenje i sabiranje kao i 
neke malo komplikovanije. Osim navedenog kreirane su metode preko Tejlorevih redova koje racunaju sinus i kosinus, a omoguceno je poredjenje rezultata ugradjenih metoda i metoda
koje sam ja kreirao.