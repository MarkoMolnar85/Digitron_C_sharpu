﻿namespace Seminarski_Digitron_Marko_Molnar_IN_56_13
{
    partial class frmGlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn7 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btnZapeta = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.txtRezultatGornji = new System.Windows.Forms.TextBox();
            this.btnPlus = new System.Windows.Forms.Button();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btnPodeljeno = new System.Windows.Forms.Button();
            this.btnPuta = new System.Windows.Forms.Button();
            this.btnJednako = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnCE = new System.Windows.Forms.Button();
            this.btnBS = new System.Windows.Forms.Button();
            this.btnPlusMinus = new System.Windows.Forms.Button();
            this.lblPrethodniBroj = new System.Windows.Forms.Label();
            this.btnReciprocno = new System.Windows.Forms.Button();
            this.btnProcenat = new System.Windows.Forms.Button();
            this.btnSqrt = new System.Windows.Forms.Button();
            this.btnFaktorijel = new System.Windows.Forms.Button();
            this.btnKvadrat = new System.Windows.Forms.Button();
            this.btnKubni = new System.Windows.Forms.Button();
            this.btnXnaNesto = new System.Windows.Forms.Button();
            this.btnLogaritam = new System.Windows.Forms.Button();
            this.btnSinus = new System.Windows.Forms.Button();
            this.btnCosinus = new System.Windows.Forms.Button();
            this.btnTangens = new System.Windows.Forms.Button();
            this.btnModOstatak = new System.Windows.Forms.Button();
            this.btn10naX = new System.Windows.Forms.Button();
            this.btnPI = new System.Windows.Forms.Button();
            this.btnINT = new System.Windows.Forms.Button();
            this.btnBinarni = new System.Windows.Forms.Button();
            this.btnExp = new System.Windows.Forms.Button();
            this.txtRezultatDonja = new System.Windows.Forms.TextBox();
            this.btnPrepisi = new System.Windows.Forms.Button();
            this.btnSinTejlor = new System.Windows.Forms.Button();
            this.btnCosTejlor = new System.Windows.Forms.Button();
            this.btnTanTejlor = new System.Windows.Forms.Button();
            this.cmbPreciznost = new System.Windows.Forms.ComboBox();
            this.lblPreciznost = new System.Windows.Forms.Label();
            this.btnEksponencijalna = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(436, 118);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(60, 60);
            this.btn7.TabIndex = 0;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(436, 184);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(60, 60);
            this.btn4.TabIndex = 0;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(436, 250);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(60, 60);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(502, 118);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(60, 60);
            this.btn8.TabIndex = 0;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(502, 250);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(60, 60);
            this.btn2.TabIndex = 0;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(502, 184);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(60, 60);
            this.btn5.TabIndex = 0;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(568, 118);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(60, 60);
            this.btn9.TabIndex = 0;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(568, 184);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(60, 60);
            this.btn6.TabIndex = 0;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnZapeta
            // 
            this.btnZapeta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZapeta.Location = new System.Drawing.Point(568, 316);
            this.btnZapeta.Name = "btnZapeta";
            this.btnZapeta.Size = new System.Drawing.Size(60, 60);
            this.btnZapeta.TabIndex = 0;
            this.btnZapeta.Text = ".";
            this.btnZapeta.UseVisualStyleBackColor = true;
            this.btnZapeta.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(436, 316);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(126, 60);
            this.btn0.TabIndex = 0;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(568, 250);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(60, 60);
            this.btn3.TabIndex = 0;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn_Click);
            // 
            // txtRezultatGornji
            // 
            this.txtRezultatGornji.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRezultatGornji.Location = new System.Drawing.Point(137, 12);
            this.txtRezultatGornji.Name = "txtRezultatGornji";
            this.txtRezultatGornji.Size = new System.Drawing.Size(694, 29);
            this.txtRezultatGornji.TabIndex = 1;
            this.txtRezultatGornji.Text = "0";
            this.txtRezultatGornji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.Location = new System.Drawing.Point(634, 316);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(60, 60);
            this.btnPlus.TabIndex = 2;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            this.btnPlus.Click += new System.EventHandler(this.btnRacunskaOperacija_Click);
            // 
            // btnMinus
            // 
            this.btnMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinus.Location = new System.Drawing.Point(634, 252);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(60, 60);
            this.btnMinus.TabIndex = 2;
            this.btnMinus.Text = "-";
            this.btnMinus.UseVisualStyleBackColor = true;
            this.btnMinus.Click += new System.EventHandler(this.btnRacunskaOperacija_Click);
            // 
            // btnPodeljeno
            // 
            this.btnPodeljeno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPodeljeno.Location = new System.Drawing.Point(634, 184);
            this.btnPodeljeno.Name = "btnPodeljeno";
            this.btnPodeljeno.Size = new System.Drawing.Size(60, 60);
            this.btnPodeljeno.TabIndex = 2;
            this.btnPodeljeno.Text = "/";
            this.btnPodeljeno.UseVisualStyleBackColor = true;
            this.btnPodeljeno.Click += new System.EventHandler(this.btnRacunskaOperacija_Click);
            // 
            // btnPuta
            // 
            this.btnPuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPuta.Location = new System.Drawing.Point(634, 118);
            this.btnPuta.Name = "btnPuta";
            this.btnPuta.Size = new System.Drawing.Size(60, 60);
            this.btnPuta.TabIndex = 2;
            this.btnPuta.Text = "*";
            this.btnPuta.UseVisualStyleBackColor = true;
            this.btnPuta.Click += new System.EventHandler(this.btnRacunskaOperacija_Click);
            // 
            // btnJednako
            // 
            this.btnJednako.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJednako.Location = new System.Drawing.Point(700, 252);
            this.btnJednako.Name = "btnJednako";
            this.btnJednako.Size = new System.Drawing.Size(60, 124);
            this.btnJednako.TabIndex = 2;
            this.btnJednako.Text = "=";
            this.btnJednako.UseVisualStyleBackColor = true;
            this.btnJednako.Click += new System.EventHandler(this.btnJednako_Click);
            // 
            // btnC
            // 
            this.btnC.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.Location = new System.Drawing.Point(568, 52);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(60, 60);
            this.btnC.TabIndex = 2;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnCE
            // 
            this.btnCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCE.Location = new System.Drawing.Point(502, 52);
            this.btnCE.Name = "btnCE";
            this.btnCE.Size = new System.Drawing.Size(60, 60);
            this.btnCE.TabIndex = 2;
            this.btnCE.Text = "CE";
            this.btnCE.UseVisualStyleBackColor = true;
            this.btnCE.Click += new System.EventHandler(this.btnCE_Click);
            // 
            // btnBS
            // 
            this.btnBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBS.Location = new System.Drawing.Point(436, 52);
            this.btnBS.Name = "btnBS";
            this.btnBS.Size = new System.Drawing.Size(60, 60);
            this.btnBS.TabIndex = 2;
            this.btnBS.Text = "BS";
            this.btnBS.UseVisualStyleBackColor = true;
            this.btnBS.Click += new System.EventHandler(this.btnBS_Click);
            // 
            // btnPlusMinus
            // 
            this.btnPlusMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlusMinus.Location = new System.Drawing.Point(634, 52);
            this.btnPlusMinus.Name = "btnPlusMinus";
            this.btnPlusMinus.Size = new System.Drawing.Size(60, 60);
            this.btnPlusMinus.TabIndex = 2;
            this.btnPlusMinus.Text = "+ -";
            this.btnPlusMinus.UseVisualStyleBackColor = true;
            this.btnPlusMinus.Click += new System.EventHandler(this.btnPlusMinus_Click);
            // 
            // lblPrethodniBroj
            // 
            this.lblPrethodniBroj.AutoSize = true;
            this.lblPrethodniBroj.Location = new System.Drawing.Point(137, 12);
            this.lblPrethodniBroj.Name = "lblPrethodniBroj";
            this.lblPrethodniBroj.Size = new System.Drawing.Size(13, 13);
            this.lblPrethodniBroj.TabIndex = 3;
            this.lblPrethodniBroj.Text = "0";
            // 
            // btnReciprocno
            // 
            this.btnReciprocno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReciprocno.Location = new System.Drawing.Point(700, 184);
            this.btnReciprocno.Name = "btnReciprocno";
            this.btnReciprocno.Size = new System.Drawing.Size(60, 60);
            this.btnReciprocno.TabIndex = 2;
            this.btnReciprocno.Text = "1/x";
            this.btnReciprocno.UseVisualStyleBackColor = true;
            this.btnReciprocno.Click += new System.EventHandler(this.btnReciprocno_Click);
            // 
            // btnProcenat
            // 
            this.btnProcenat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcenat.Location = new System.Drawing.Point(700, 118);
            this.btnProcenat.Name = "btnProcenat";
            this.btnProcenat.Size = new System.Drawing.Size(60, 60);
            this.btnProcenat.TabIndex = 4;
            this.btnProcenat.Text = "%";
            this.btnProcenat.UseVisualStyleBackColor = true;
            this.btnProcenat.Click += new System.EventHandler(this.btnProcenat_Click);
            // 
            // btnSqrt
            // 
            this.btnSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSqrt.Location = new System.Drawing.Point(700, 52);
            this.btnSqrt.Name = "btnSqrt";
            this.btnSqrt.Size = new System.Drawing.Size(60, 60);
            this.btnSqrt.TabIndex = 4;
            this.btnSqrt.Text = "SQRT";
            this.btnSqrt.UseVisualStyleBackColor = true;
            this.btnSqrt.Click += new System.EventHandler(this.btnSqrt_Click);
            // 
            // btnFaktorijel
            // 
            this.btnFaktorijel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFaktorijel.Location = new System.Drawing.Point(370, 52);
            this.btnFaktorijel.Name = "btnFaktorijel";
            this.btnFaktorijel.Size = new System.Drawing.Size(60, 60);
            this.btnFaktorijel.TabIndex = 2;
            this.btnFaktorijel.Text = "n!";
            this.btnFaktorijel.UseVisualStyleBackColor = true;
            this.btnFaktorijel.Click += new System.EventHandler(this.btnFaktorijel_Click);
            // 
            // btnKvadrat
            // 
            this.btnKvadrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKvadrat.Location = new System.Drawing.Point(370, 118);
            this.btnKvadrat.Name = "btnKvadrat";
            this.btnKvadrat.Size = new System.Drawing.Size(60, 60);
            this.btnKvadrat.TabIndex = 2;
            this.btnKvadrat.Text = "X^2";
            this.btnKvadrat.UseVisualStyleBackColor = true;
            this.btnKvadrat.Click += new System.EventHandler(this.btnKvadrat_Click);
            // 
            // btnKubni
            // 
            this.btnKubni.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKubni.Location = new System.Drawing.Point(370, 184);
            this.btnKubni.Name = "btnKubni";
            this.btnKubni.Size = new System.Drawing.Size(60, 60);
            this.btnKubni.TabIndex = 2;
            this.btnKubni.Text = "X^3";
            this.btnKubni.UseVisualStyleBackColor = true;
            this.btnKubni.Click += new System.EventHandler(this.btnKubni_Click);
            // 
            // btnXnaNesto
            // 
            this.btnXnaNesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXnaNesto.Location = new System.Drawing.Point(370, 250);
            this.btnXnaNesto.Name = "btnXnaNesto";
            this.btnXnaNesto.Size = new System.Drawing.Size(60, 60);
            this.btnXnaNesto.TabIndex = 2;
            this.btnXnaNesto.Text = "X^y";
            this.btnXnaNesto.UseVisualStyleBackColor = true;
            this.btnXnaNesto.Click += new System.EventHandler(this.btnXnaNesto_Click);
            // 
            // btnLogaritam
            // 
            this.btnLogaritam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogaritam.Location = new System.Drawing.Point(370, 316);
            this.btnLogaritam.Name = "btnLogaritam";
            this.btnLogaritam.Size = new System.Drawing.Size(60, 60);
            this.btnLogaritam.TabIndex = 2;
            this.btnLogaritam.Text = "LOG";
            this.btnLogaritam.UseVisualStyleBackColor = true;
            this.btnLogaritam.Click += new System.EventHandler(this.btnLogaritam_Click);
            // 
            // btnSinus
            // 
            this.btnSinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSinus.Location = new System.Drawing.Point(304, 52);
            this.btnSinus.Name = "btnSinus";
            this.btnSinus.Size = new System.Drawing.Size(60, 60);
            this.btnSinus.TabIndex = 5;
            this.btnSinus.Text = "sin";
            this.btnSinus.UseVisualStyleBackColor = true;
            this.btnSinus.Click += new System.EventHandler(this.btnSinus_Click);
            // 
            // btnCosinus
            // 
            this.btnCosinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCosinus.Location = new System.Drawing.Point(304, 118);
            this.btnCosinus.Name = "btnCosinus";
            this.btnCosinus.Size = new System.Drawing.Size(60, 60);
            this.btnCosinus.TabIndex = 5;
            this.btnCosinus.Text = "cos";
            this.btnCosinus.UseVisualStyleBackColor = true;
            this.btnCosinus.Click += new System.EventHandler(this.btnCosinus_Click);
            // 
            // btnTangens
            // 
            this.btnTangens.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTangens.Location = new System.Drawing.Point(304, 184);
            this.btnTangens.Name = "btnTangens";
            this.btnTangens.Size = new System.Drawing.Size(60, 60);
            this.btnTangens.TabIndex = 5;
            this.btnTangens.Text = "tan";
            this.btnTangens.UseVisualStyleBackColor = true;
            this.btnTangens.Click += new System.EventHandler(this.btnTangens_Click);
            // 
            // btnModOstatak
            // 
            this.btnModOstatak.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModOstatak.Location = new System.Drawing.Point(304, 250);
            this.btnModOstatak.Name = "btnModOstatak";
            this.btnModOstatak.Size = new System.Drawing.Size(60, 60);
            this.btnModOstatak.TabIndex = 5;
            this.btnModOstatak.Text = "Mod";
            this.btnModOstatak.UseVisualStyleBackColor = true;
            this.btnModOstatak.Click += new System.EventHandler(this.btnModOstatak_Click);
            // 
            // btn10naX
            // 
            this.btn10naX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn10naX.Location = new System.Drawing.Point(304, 316);
            this.btn10naX.Name = "btn10naX";
            this.btn10naX.Size = new System.Drawing.Size(60, 60);
            this.btn10naX.TabIndex = 5;
            this.btn10naX.Text = "10^x";
            this.btn10naX.UseVisualStyleBackColor = true;
            this.btn10naX.Click += new System.EventHandler(this.desetNaX);
            // 
            // btnPI
            // 
            this.btnPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPI.Location = new System.Drawing.Point(238, 52);
            this.btnPI.Name = "btnPI";
            this.btnPI.Size = new System.Drawing.Size(60, 60);
            this.btnPI.TabIndex = 6;
            this.btnPI.Text = "PI";
            this.btnPI.UseVisualStyleBackColor = true;
            this.btnPI.Click += new System.EventHandler(this.btnPI_Click);
            // 
            // btnINT
            // 
            this.btnINT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnINT.Location = new System.Drawing.Point(238, 252);
            this.btnINT.Name = "btnINT";
            this.btnINT.Size = new System.Drawing.Size(60, 124);
            this.btnINT.TabIndex = 6;
            this.btnINT.Text = "INT";
            this.btnINT.UseVisualStyleBackColor = true;
            this.btnINT.Click += new System.EventHandler(this.btnINT_Click);
            // 
            // btnBinarni
            // 
            this.btnBinarni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBinarni.Location = new System.Drawing.Point(238, 184);
            this.btnBinarni.Name = "btnBinarni";
            this.btnBinarni.Size = new System.Drawing.Size(60, 60);
            this.btnBinarni.TabIndex = 7;
            this.btnBinarni.Text = "BIN";
            this.btnBinarni.UseVisualStyleBackColor = true;
            this.btnBinarni.Click += new System.EventHandler(this.btnBinarni_Click);
            // 
            // btnExp
            // 
            this.btnExp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExp.Location = new System.Drawing.Point(238, 118);
            this.btnExp.Name = "btnExp";
            this.btnExp.Size = new System.Drawing.Size(60, 60);
            this.btnExp.TabIndex = 6;
            this.btnExp.Text = "Exp";
            this.btnExp.UseVisualStyleBackColor = true;
            this.btnExp.Click += new System.EventHandler(this.btnExp_Click);
            // 
            // txtRezultatDonja
            // 
            this.txtRezultatDonja.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRezultatDonja.Location = new System.Drawing.Point(156, 401);
            this.txtRezultatDonja.Name = "txtRezultatDonja";
            this.txtRezultatDonja.Size = new System.Drawing.Size(654, 29);
            this.txtRezultatDonja.TabIndex = 8;
            this.txtRezultatDonja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPrepisi
            // 
            this.btnPrepisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrepisi.Location = new System.Drawing.Point(715, 436);
            this.btnPrepisi.Name = "btnPrepisi";
            this.btnPrepisi.Size = new System.Drawing.Size(95, 38);
            this.btnPrepisi.TabIndex = 9;
            this.btnPrepisi.Text = "Prepisi";
            this.btnPrepisi.UseVisualStyleBackColor = true;
            this.btnPrepisi.Click += new System.EventHandler(this.btnPrepisi_Click);
            // 
            // btnSinTejlor
            // 
            this.btnSinTejlor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSinTejlor.Location = new System.Drawing.Point(156, 436);
            this.btnSinTejlor.Name = "btnSinTejlor";
            this.btnSinTejlor.Size = new System.Drawing.Size(69, 38);
            this.btnSinTejlor.TabIndex = 9;
            this.btnSinTejlor.Text = "SIN";
            this.btnSinTejlor.UseVisualStyleBackColor = true;
            this.btnSinTejlor.Click += new System.EventHandler(this.trigonometrijaTejlor);
            // 
            // btnCosTejlor
            // 
            this.btnCosTejlor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCosTejlor.Location = new System.Drawing.Point(233, 436);
            this.btnCosTejlor.Name = "btnCosTejlor";
            this.btnCosTejlor.Size = new System.Drawing.Size(69, 38);
            this.btnCosTejlor.TabIndex = 9;
            this.btnCosTejlor.Text = "COS";
            this.btnCosTejlor.UseVisualStyleBackColor = true;
            this.btnCosTejlor.Click += new System.EventHandler(this.trigonometrijaTejlor);
            // 
            // btnTanTejlor
            // 
            this.btnTanTejlor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTanTejlor.Location = new System.Drawing.Point(308, 436);
            this.btnTanTejlor.Name = "btnTanTejlor";
            this.btnTanTejlor.Size = new System.Drawing.Size(69, 38);
            this.btnTanTejlor.TabIndex = 9;
            this.btnTanTejlor.Text = "TAN";
            this.btnTanTejlor.UseVisualStyleBackColor = true;
            this.btnTanTejlor.Click += new System.EventHandler(this.trigonometrijaTejlor);
            // 
            // cmbPreciznost
            // 
            this.cmbPreciznost.FormattingEnabled = true;
            this.cmbPreciznost.Items.AddRange(new object[] {
            "5",
            "10",
            "15",
            "20",
            ""});
            this.cmbPreciznost.Location = new System.Drawing.Point(588, 436);
            this.cmbPreciznost.Name = "cmbPreciznost";
            this.cmbPreciznost.Size = new System.Drawing.Size(106, 21);
            this.cmbPreciznost.TabIndex = 10;
            this.cmbPreciznost.Text = "5";
            // 
            // lblPreciznost
            // 
            this.lblPreciznost.AutoSize = true;
            this.lblPreciznost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreciznost.Location = new System.Drawing.Point(513, 437);
            this.lblPreciznost.Name = "lblPreciznost";
            this.lblPreciznost.Size = new System.Drawing.Size(69, 16);
            this.lblPreciznost.TabIndex = 11;
            this.lblPreciznost.Text = "preciznost";
            // 
            // btnEksponencijalna
            // 
            this.btnEksponencijalna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEksponencijalna.Location = new System.Drawing.Point(383, 435);
            this.btnEksponencijalna.Name = "btnEksponencijalna";
            this.btnEksponencijalna.Size = new System.Drawing.Size(69, 38);
            this.btnEksponencijalna.TabIndex = 12;
            this.btnEksponencijalna.Text = "e^x";
            this.btnEksponencijalna.UseVisualStyleBackColor = true;
            this.btnEksponencijalna.Click += new System.EventHandler(this.eksponencijalnaEnaX);
            // 
            // frmGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 516);
            this.Controls.Add(this.btnEksponencijalna);
            this.Controls.Add(this.lblPreciznost);
            this.Controls.Add(this.cmbPreciznost);
            this.Controls.Add(this.btnSinTejlor);
            this.Controls.Add(this.btnCosTejlor);
            this.Controls.Add(this.btnTanTejlor);
            this.Controls.Add(this.btnPrepisi);
            this.Controls.Add(this.txtRezultatDonja);
            this.Controls.Add(this.btnBinarni);
            this.Controls.Add(this.btnINT);
            this.Controls.Add(this.btnExp);
            this.Controls.Add(this.btnPI);
            this.Controls.Add(this.btn10naX);
            this.Controls.Add(this.btnModOstatak);
            this.Controls.Add(this.btnTangens);
            this.Controls.Add(this.btnCosinus);
            this.Controls.Add(this.btnSinus);
            this.Controls.Add(this.btnSqrt);
            this.Controls.Add(this.btnProcenat);
            this.Controls.Add(this.lblPrethodniBroj);
            this.Controls.Add(this.btnReciprocno);
            this.Controls.Add(this.btnPlusMinus);
            this.Controls.Add(this.btnLogaritam);
            this.Controls.Add(this.btnXnaNesto);
            this.Controls.Add(this.btnKubni);
            this.Controls.Add(this.btnKvadrat);
            this.Controls.Add(this.btnFaktorijel);
            this.Controls.Add(this.btnBS);
            this.Controls.Add(this.btnCE);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnPuta);
            this.Controls.Add(this.btnPodeljeno);
            this.Controls.Add(this.btnMinus);
            this.Controls.Add(this.btnJednako);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.txtRezultatGornji);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btnZapeta);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn7);
            this.Name = "frmGlavnaForma";
            this.ShowIcon = false;
            this.Text = "Digitron";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btnZapeta;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.TextBox txtRezultatGornji;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button btnMinus;
        private System.Windows.Forms.Button btnPodeljeno;
        private System.Windows.Forms.Button btnPuta;
        private System.Windows.Forms.Button btnJednako;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnCE;
        private System.Windows.Forms.Button btnBS;
        private System.Windows.Forms.Button btnPlusMinus;
        private System.Windows.Forms.Label lblPrethodniBroj;
        private System.Windows.Forms.Button btnReciprocno;
        private System.Windows.Forms.Button btnProcenat;
        private System.Windows.Forms.Button btnSqrt;
        private System.Windows.Forms.Button btnFaktorijel;
        private System.Windows.Forms.Button btnKvadrat;
        private System.Windows.Forms.Button btnKubni;
        private System.Windows.Forms.Button btnXnaNesto;
        private System.Windows.Forms.Button btnLogaritam;
        private System.Windows.Forms.Button btnSinus;
        private System.Windows.Forms.Button btnCosinus;
        private System.Windows.Forms.Button btnTangens;
        private System.Windows.Forms.Button btnModOstatak;
        private System.Windows.Forms.Button btn10naX;
        private System.Windows.Forms.Button btnPI;
        private System.Windows.Forms.Button btnINT;
        private System.Windows.Forms.Button btnBinarni;
        private System.Windows.Forms.Button btnExp;
        private System.Windows.Forms.TextBox txtRezultatDonja;
        private System.Windows.Forms.Button btnPrepisi;
        private System.Windows.Forms.Button btnSinTejlor;
        private System.Windows.Forms.Button btnCosTejlor;
        private System.Windows.Forms.Button btnTanTejlor;
        private System.Windows.Forms.ComboBox cmbPreciznost;
        private System.Windows.Forms.Label lblPreciznost;
        private System.Windows.Forms.Button btnEksponencijalna;
    }
}

