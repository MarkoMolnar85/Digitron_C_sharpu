﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seminarski_Digitron_Marko_Molnar_IN_56_13
{
    public partial class frmGlavnaForma : Form                 //glavna forma
    {
        double vrednostPrvogGlob=0;
        double vrednostDrugogGlob=0;
        string osnovnaRacunskaOperacija = "";
        bool osnovnaRacOpKliknuta = false;
        bool stepenovanjeKliknuto = false;
        public frmGlavnaForma()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, EventArgs e)                           //dugmad brojevi
        {
            Button dugme = (Button)sender;
            if (txtRezultatGornji.Text == "0" || osnovnaRacOpKliknuta == true || stepenovanjeKliknuto == true)
            {
                txtRezultatGornji.Clear();
            }
            txtRezultatGornji.Text = txtRezultatGornji.Text + dugme.Text;
            if (txtRezultatGornji.Text != ".")
                vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            else
                vrednostDrugogGlob = double.Parse("0" + txtRezultatGornji.Text);
            osnovnaRacOpKliknuta = false;
            stepenovanjeKliknuto = false;
        }

        private void btnRacunskaOperacija_Click(object sender, EventArgs e)         //osnovne racunske operacije +-*/
        {
            osnovnaRacOpKliknuta = true;
            Button dugmeRacunskaOperacija = (Button)sender;
            osnovnaRacunskaOperacija = dugmeRacunskaOperacija.Text;
            vrednostPrvogGlob = double.Parse(txtRezultatGornji.Text);
            lblPrethodniBroj.Text = vrednostPrvogGlob + osnovnaRacunskaOperacija;
        }

        private void btnJednako_Click(object sender, EventArgs e)                // jednako je
        {
            lblPrethodniBroj.Text = "";
            double rezultat=0;
            switch (osnovnaRacunskaOperacija)
            {
                case "+":
                    rezultat = vrednostPrvogGlob + vrednostDrugogGlob;
                    break;
                case "-":
                    rezultat = vrednostPrvogGlob - vrednostDrugogGlob;
                    break;
                case "*":
                    rezultat = vrednostDrugogGlob * vrednostPrvogGlob;
                    break;
                case "/":
                    rezultat = vrednostPrvogGlob / vrednostDrugogGlob;
                    break;
                case "^":
                    rezultat = Math.Pow(vrednostPrvogGlob, vrednostDrugogGlob);
                    break;
                case "%":
                    rezultat = vrednostPrvogGlob % vrednostDrugogGlob;
                    break;
                case "EXP":
                    rezultat = exp(vrednostPrvogGlob, vrednostDrugogGlob);
                    break;
                default:
                    break;
            }
            txtRezultatGornji.Text = rezultat.ToString();
        }

        private void btnC_Click(object sender, EventArgs e)                //brisanje celog
        {
            txtRezultatGornji.Text = "0";
            lblPrethodniBroj.Text = "";
            vrednostPrvogGlob = 0;
            vrednostDrugogGlob = 0;
        }

        private void btnCE_Click(object sender, EventArgs e)               //brisanje zadnjeg broja
        {
            vrednostDrugogGlob = 0;
            txtRezultatGornji.Text = "0";
        }

        private void btnBS_Click(object sender, EventArgs e)              //skidanje zadnje cifre treba doraditi
        {
            int skraceni;
            vrednostDrugogGlob = vrednostDrugogGlob/10;
            skraceni = (int)vrednostDrugogGlob;
            vrednostDrugogGlob = (double)skraceni;
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnPlusMinus_Click(object sender, EventArgs e)                 // pozitivan negativan broj
        {
            if (vrednostDrugogGlob > 0)
            {
                vrednostDrugogGlob = -vrednostDrugogGlob;
                txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
            }
            else
            {
                vrednostDrugogGlob = vrednostDrugogGlob * -1;
                txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
            }
        }

        private void btnReciprocno_Click(object sender, EventArgs e)                  //reciprocno
        {
            vrednostDrugogGlob = 1 / vrednostDrugogGlob;
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnProcenat_Click(object sender, EventArgs e)                              //procenat
        {
            vrednostDrugogGlob = vrednostDrugogGlob / 100 * vrednostPrvogGlob;

        }

        private void btnSqrt_Click(object sender, EventArgs e)                              //koren preko fje ugradjene
        {
            vrednostDrugogGlob = Math.Sqrt(vrednostDrugogGlob);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnFaktorijel_Click(object sender, EventArgs e)                   //faktorijel   odozgo na dole
        {
            int i;
            int limitBrojaca = (int)vrednostDrugogGlob;
            double dobijeniFaktorijel =1;
            for (i = limitBrojaca; i > 0; i--)
            {
                dobijeniFaktorijel = dobijeniFaktorijel * i;
            }
            vrednostDrugogGlob = dobijeniFaktorijel;
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnKvadrat_Click(object sender, EventArgs e)                 //kvadrat
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            vrednostDrugogGlob = Math.Pow(vrednostDrugogGlob, 2);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();                
        }

        private void btnKubni_Click(object sender, EventArgs e)                 //kub
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            vrednostDrugogGlob = Math.Pow(vrednostDrugogGlob, 3);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnXnaNesto_Click(object sender, EventArgs e)                    //stepenovanje sa unosenjem stepena
        {
            stepenovanjeKliknuto = true;
            vrednostPrvogGlob = double.Parse(txtRezultatGornji.Text);
            osnovnaRacunskaOperacija = "^";
            lblPrethodniBroj.Text = vrednostPrvogGlob.ToString() + osnovnaRacunskaOperacija;
        }

        private void btnLogaritam_Click(object sender, EventArgs e)                      //logaritam
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            vrednostDrugogGlob = Math.Log10(vrednostDrugogGlob);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnSinus_Click(object sender, EventArgs e)                         //sinus
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            double pomocna = Math.PI / 180 * vrednostDrugogGlob;
            vrednostDrugogGlob = Math.Sin(pomocna);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnCosinus_Click(object sender, EventArgs e)                //cosinus 
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            double rezultat;
            double pomocna = vrednostDrugogGlob * Math.PI / 180;
            rezultat = Math.Cos(pomocna);
            
            txtRezultatGornji.Text = rezultat.ToString();
        }

        private void btnTangens_Click(object sender, EventArgs e)               //tangens 
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            double pomocna = Math.PI / 180 * vrednostDrugogGlob;
            vrednostDrugogGlob = Math.Tan(pomocna);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnPI_Click(object sender, EventArgs e)                           //PI
        {
            vrednostDrugogGlob = Math.PI;
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnINT_Click(object sender, EventArgs e)                           //INT
        {
            int intiger = (int)vrednostDrugogGlob;
            txtRezultatGornji.Text = intiger.ToString();
        }

        private void btnBinarni_Click(object sender, EventArgs e)            //dekadni u binarni 
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            string binarni = "";
            int[] A;
            int dekadni = (int)vrednostDrugogGlob;
            int zaDeljenje = (int)vrednostDrugogGlob;
            int i=0;
            while (dekadni != 0)
            {
                dekadni = dekadni / 2;
                i++;
            }
            A = new int[i];
            int j = 0;
            for (j = 0; j < i; j++)
            {
                A[j] = zaDeljenje % 2;
                zaDeljenje = zaDeljenje / 2;
            }
            int k;
            for (k = i-1; k >= 0; k--)
            {
                binarni = binarni + A[k].ToString();
            }
            txtRezultatGornji.Text = binarni;
            i = j = k = 0;
        }

        private void btnModOstatak_Click(object sender, EventArgs e)          //mod ostatak
        {
            osnovnaRacOpKliknuta = true;
            osnovnaRacunskaOperacija = "%";
            vrednostPrvogGlob = double.Parse(txtRezultatGornji.Text);
        }
        private void desetNaX(object sender, EventArgs e)
        {
            vrednostDrugogGlob = double.Parse(txtRezultatGornji.Text);
            vrednostDrugogGlob = Math.Pow(10, vrednostDrugogGlob);
            txtRezultatGornji.Text = vrednostDrugogGlob.ToString();
        }

        private void btnExp_Click(object sender, EventArgs e)                //btn exp
        {
            stepenovanjeKliknuto = true;
            vrednostPrvogGlob = double.Parse(txtRezultatGornji.Text);
            osnovnaRacunskaOperacija = "EXP";
            lblPrethodniBroj.Text = vrednostDrugogGlob.ToString() + osnovnaRacunskaOperacija;
        }
        static double exp(double x, double n)              //pomocna fja za exp
        {
            int i;
            for (i = 0; i < n; i++)
            {
                x = x * 10;
            }
            return x;
        }
        static double FaktorijelFja(double fakt)                     //pomocna fja faktorijel
        {
            int n = (int)fakt;
            fakt = 1;
            if (n != 0 || n != 1)
            {
                for (int i = 1; i <= n; i++)
                {
                    fakt = fakt * i;
                }
            }
            else
                fakt = 1;
            return fakt;
        }
        static double sinusFja(double x, int n)                   //pomocna fja sinus taylor
        {
            x = Math.PI / 180 * x;
            x = x % (2 * Math.PI);
            double sum = 0;
            int j = 0;
            int k = 1; ;

            for (int i = 0; i < n; i++)
            {

                if (j % 2 == 0)
                {
                    sum = sum + (Math.Pow(x, k) / FaktorijelFja(k));
                }
                else
                {
                    sum = sum - (Math.Pow(x, k) / FaktorijelFja(k));
                }
                k = k + 2;
                j++;
            }
            return sum;
        }
        static double kosinusFja(double x, int n)             //pomocna fja kosinus taylor
        {
            x = Math.PI / 180 * x;
            x = x % (2 * Math.PI);
            double sum = 0;
            int j = 0;
            int k = 0;
            for (int i = 0; i < n; i++)
            {
                if (j % 2 == 0)
                {
                    sum = sum + (Math.Pow(x, k) / FaktorijelFja(k));
                }
                else
                {
                    sum = sum - (Math.Pow(x, k) / FaktorijelFja(k));
                }
                j++;
                k += 2;
            }
            return sum;
        }
        static double tangensFja(double x, int n)                        //pomocna fja tanfens tajlot sin / cos
        {
            double tangensX;
            tangensX = sinusFja(x, n) / kosinusFja(x, n);
            return tangensX;
        }

        private void btnPrepisi_Click(object sender, EventArgs e)                //event prepisivanje gornjeg u donji txtbox
        {
            txtRezultatDonja.Text = txtRezultatGornji.Text;
        }
        private void trigonometrijaTejlor(object sender, EventArgs e)             //trigonometrija tejlor
        {
            Button b = (Button)sender;
            double x;
            int n = int.Parse(cmbPreciznost.Text);
            x = double.Parse(txtRezultatDonja.Text);
            double rezultat = 0;
            if (b.Text == "SIN")
            {
                rezultat = sinusFja(x, n);
            }
            else if (b.Text == "COS")
            {
                rezultat = kosinusFja(x, n);
            }
            else if (b.Text == "TAN")
            {
                rezultat = tangensFja(x, n);
            }
            txtRezultatDonja.Text = rezultat.ToString();

        }
        static double eNaXFja(double x, int n)           //pomocna eksponencijalna
        {
            double ex = 1;
            double clan = 1;
            int i;
            for (i = 1; i < n; i++)
            {
                clan *= x / i;
                ex += clan;
            }
            return ex;
        }
        static double eNaXFjaFaktorijalem(double x, int n)          //pomocna eksponencijalna ss faktorijalem
        {
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += ((Math.Pow(x, i)) / FaktorijelFja(i));
            }
                return sum;
        }
        private void eksponencijalnaEnaX(object sender, EventArgs e)               //eksponencijalna e na x
        {
            double x;
            int n = int.Parse(cmbPreciznost.Text);
            x = double.Parse(txtRezultatDonja.Text);
            double rezultat;
            rezultat = eNaXFjaFaktorijalem(x, n);
            txtRezultatDonja.Text = rezultat.ToString();
            x = 0;
        }











  









    }
}
